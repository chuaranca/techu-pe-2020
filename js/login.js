function init() {
    console.log('Inicio de la función init()');
    /* Como primer paso, se debe de registrar el user y pass en el storage, sólo 
    como fines educativos... para posterior hacer una simulación de datos correctos*/
    sessionStorage.setItem('user', 'chayanne');
    sessionStorage.setItem('pass', '123456');
}

function login() {
    console.log('Inicio de la función login()');
    let user = document.getElementById('input-user');
    let pass = document.getElementById('input-password');

    let user_local = sessionStorage.getItem('user');
    let pass_local = sessionStorage.getItem('pass');

    //console.log(`user: ${user} || password: ${pass}`);
    if (user.value == user_local && pass.value == pass_local) {
        alert('Usuario y Password CORRECTO!');
        location.href = 'test.html'
    } else {
        alert('Usuario o Password INCORRECTO');
        user.value = "";
        pass.value = "";
    }

}